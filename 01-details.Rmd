# Details {#details}

A comprehensive documentation of `GitLab Pages` can be found here: https://docs.gitlab.com/ee/user/project/pages.

## `GitLab CI` configuration file {#CI-file-details}

`GitLab CI` configuration file is a `YAML` file.  
The configuration file begins with `image` declaration. It indicates to the `GitLab Runner` which [`Docker`](https://www.docker.com/) image it has to pull for executing the `CI` job.

```{r comment=NA, class.output='yaml', echo=FALSE}
image <- 
  readLines("./.gitlab-ci.yml") %>%
  stringr::str_subset("image") %T>%
  cat() %>%
  yaml::yaml.load() %$%
  image %>%
  stringr::str_split(":") %>%
  unlist()
```

In order to build a website with `bookdown`, you have to choose a [`Docker`](https://www.docker.com/) image with `bookdown` installed. In this example, the `r image[1] %>% paste0("[\u0060", ., "\u0060](https://hub.docker.com/r/", ., ")")` image is used: [`Rocker`](https://www.rocker-project.org/) is the [official `R` image for `Docker`](http://dirk.eddelbuettel.com/blog/2014/12/19/). I am a newbie with [`Docker`](https://www.docker.com/) and I recommend this [rOpenSci](https://ropensci.org/)'s [Docker tutorial for reproducible research](http://ropenscilabs.github.io/r-docker-tutorial/). 

Using a `Rocker` image, **you ensure the reproducibility of your `bookdown` website**.

In order to build a `GitLab Pages` website, you have to declare a `pages` job. That is the remaining lines of the `.gitlab-ci.yml` file:

```{r comment=NA, class.output='yaml', echo=FALSE}
readChar("./.gitlab-ci.yml", 1e5) %>%
  strsplit("(?=\\npages:)", perl = TRUE) %>%
  unlist() %>%
  stringr::str_subset("pages") %>%
  cat()
```

In this example, there is only a `deploy` stage, but you can add a `test` stage on any branch.

## LaTeX engine

At the time this demo guide is written, there is only one pitfall: the `rocker/verse` image does not include `xelatex` `pdf` engine.  
So, you have to use `pdflatex` instead of `xelatex`.

